# ubuntu

ARG UBUNTU_VERSION=18.04


FROM ubuntu:${UBUNTU_VERSION} AS ubuntu

ENV \
	DEBIAN_FRONTEND=noninteractive \
	container=docker

WORKDIR /tmp

COPY \
	./files/ \
	./

RUN \
	cp /tmp/etc/apt/apt.conf.d/docker-* /etc/apt/apt.conf.d/ ; \
	chmod 0644 /etc/apt/apt.conf.d/docker-* ; \
	apt-get -q -y update ; \
	apt-get -q -y install \
		ca-certificates \
		curl \
		jq \
		rsync \
	; \
	apt-get -q -y clean ; \
	find /var/lib/apt/lists/ -type f -delete
